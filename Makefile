#! /usr/bin/make -f
# SPDX-License-Identifier: AGPL-3.0-or-later
SPDX_License_Identifier=AGPL-3.0-or-later
PORT=1027
.PHONY: all install  start stop uninstall
all:
	exit 64
start: install
	systemctl --user start echod@$(PORT).socket
install:
	systemctl --user enable `pwd`/echod@.service
	systemctl --user enable `pwd`/echod@.socket
uninstall: stop
	systemctl --user disable echod@.socket
	systemctl --user disable echod@.service
stop:
	systemctl --user stop echod@$(PORT).socket
